-- MySQL Workbench Forward Engineering

-- -----------------------------------------------------
-- Schema project
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `project` DEFAULT CHARACTER SET utf8 ;
USE `project` ;

-- -----------------------------------------------------
-- Table `project`.`contact`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project`.`contact` (
  `contact_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `phone_number` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NULL,
  PRIMARY KEY (`contact_id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `project`.`insurance_company`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project`.`insurance_company` (
  `insurance_company_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `insurance_company` VARCHAR(45) NOT NULL,
  `insurance_company_code` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`insurance_company_id`))
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `project`.`patient`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project`.`patient` (
  `patient_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(45) NOT NULL,
  `date_of_birth` DATE NOT NULL,
  `personal_no` VARCHAR(45) NOT NULL,
  `contact_id` INT UNSIGNED NOT NULL,
  `insurance_company_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`patient_id`, `contact_id`, `insurance_company_id`),
  INDEX `fk_patient_contact1_idx` (`contact_id` ASC),
  INDEX `fk_patient_insurance_company1_idx` (`insurance_company_id` ASC),
  CONSTRAINT `fk_patient_contact1`
    FOREIGN KEY (`contact_id`)
    REFERENCES `project`.`contact` (`contact_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_patient_insurance_company1`
    FOREIGN KEY (`insurance_company_id`)
    REFERENCES `project`.`insurance_company` (`insurance_company_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `project`.`doctor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project`.`doctor` (
  `doctor_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `doctor_name` VARCHAR(45) NOT NULL,
  `doctor_surname` VARCHAR(45) NOT NULL,
  `contact_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`doctor_id`, `contact_id`),
  INDEX `fk_doctor_contact1_idx` (`contact_id` ASC),
  CONSTRAINT `fk_doctor_contact1`
    FOREIGN KEY (`contact_id`)
    REFERENCES `project`.`contact` (`contact_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `project`.`patient_has_doctor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project`.`patient_has_doctor` (
  `patient_id` INT UNSIGNED NOT NULL,
  `doctor_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`patient_id`, `doctor_id`),
  INDEX `fk_patient_has_doctor_doctor1_idx` (`doctor_id` ASC),
  INDEX `fk_patient_has_doctor_patient1_idx` (`patient_id` ASC),
  CONSTRAINT `fk_patient_has_doctor_patient1`
    FOREIGN KEY (`patient_id`)
    REFERENCES `project`.`patient` (`patient_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_patient_has_doctor_doctor1`
    FOREIGN KEY (`doctor_id`)
    REFERENCES `project`.`doctor` (`doctor_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `project`.`diagnose`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project`.`diagnose` (
  `diagnose_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `diagnose` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`diagnose_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `project`.`patient_has_diagnose`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project`.`patient_has_diagnose` (
  `patient_id` INT UNSIGNED NOT NULL,
  `diagnose_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`diagnose_id`, `patient_id`),
  INDEX `fk_patient_has_diagnose_diagnose1_idx` (`diagnose_id` ASC),
  INDEX `fk_patient_has_diagnose_patient1_idx` (`patient_id` ASC),
  CONSTRAINT `fk_patient_has_diagnose_patient1`
    FOREIGN KEY (`patient_id`)
    REFERENCES `project`.`patient` (`patient_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_patient_has_diagnose_diagnose1`
    FOREIGN KEY (`diagnose_id`)
    REFERENCES `project`.`diagnose` (`diagnose_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `project`.`department`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project`.`department` (
  `department_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `department` VARCHAR(45) NOT NULL,
  `floor` INT NULL,
  PRIMARY KEY (`department_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `project`.`patient_in_department`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project`.`patient_in_department` (
  `patient_id` INT UNSIGNED NOT NULL,
  `department_id` INT UNSIGNED NOT NULL,
  `date` DATE NOT NULL,
  PRIMARY KEY (`patient_id`, `department_id`),
  INDEX `fk_patient_has_department_department1_idx` (`department_id` ASC),
  INDEX `fk_patient_has_department_patient1_idx` (`patient_id` ASC),
  CONSTRAINT `fk_patient_has_department_patient1`
    FOREIGN KEY (`patient_id`)
    REFERENCES `project`.`patient` (`patient_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_patient_has_department_department1`
    FOREIGN KEY (`department_id`)
    REFERENCES `project`.`department` (`department_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `project`.`address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project`.`address` (
  `address_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `city` VARCHAR(45) NOT NULL,
  `street` VARCHAR(45) NOT NULL,
  `house_number` VARCHAR(45) NOT NULL,
  `zip_code` INT NOT NULL,
  PRIMARY KEY (`address_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `project`.`patient_has_address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project`.`patient_has_address` (
  `patient_id` INT UNSIGNED NOT NULL,
  `address_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`patient_id`, `address_id`),
  INDEX `fk_patient_has_address_address1_idx` (`address_id` ASC),
  INDEX `fk_patient_has_address_patient1_idx` (`patient_id` ASC),
  CONSTRAINT `fk_patient_has_address_patient1`
    FOREIGN KEY (`patient_id`)
    REFERENCES `project`.`patient` (`patient_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_patient_has_address_address1`
    FOREIGN KEY (`address_id`)
    REFERENCES `project`.`address` (`address_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `project`.`hospital`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project`.`hospital` (
  `hospital_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `hospital_name` VARCHAR(45) NOT NULL,
  `address_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`hospital_id`, `address_id`),
  INDEX `fk_hospital_address1_idx` (`address_id` ASC),
  CONSTRAINT `fk_hospital_address1`
    FOREIGN KEY (`address_id`)
    REFERENCES `project`.`address` (`address_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `project`.`hospital_has_patient`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `project`.`hospital_has_patient` (
  `hospital_id` INT UNSIGNED NOT NULL,
  `patient_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`hospital_id`, `patient_id`),
  INDEX `fk_hospital_has_patient_patient1_idx` (`patient_id` ASC),
  INDEX `fk_hospital_has_patient_hospital1_idx` (`hospital_id` ASC),
  CONSTRAINT `fk_hospital_has_patient_hospital1`
    FOREIGN KEY (`hospital_id`)
    REFERENCES `project`.`hospital` (`hospital_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_hospital_has_patient_patient1`
    FOREIGN KEY (`patient_id`)
    REFERENCES `project`.`patient` (`patient_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
