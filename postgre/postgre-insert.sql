-- 51 kontaktů (51 na pacienty +  28 na doktory)
-- 51 pacientů
-- 7 pojišťoven
-- 51 adres (města (14): Brno, České Budějovice, Hradec Králové, Jihlava, Kladno, Liberec, Most, Olomouc, Ostrava, Opava, Pardubice, Plzeň, Praha, Zlín)
-- 14 nemocnic
-- 28 doktorů
-- 12 diagnoz
-- 11 oddělení 

--kód pro ověření:
SELECT p.patient_id, name, surname, city, street, hospital_name, insurance_company, phone_number FROM patient p
JOIN address a ON p.patient_id = a.address_id
JOIN hospital_has_patient q ON p.patient_id=q.patient_id
JOIN hospital h ON h.hospital_id=q.Hospital_id
JOIN insurance_company i ON i.insurance_company_id = p.insurance_company_id
JOIN contact c ON c.contact_id=p.contact_id

--pro vše:
SELECT p.patient_id, name, surname, city, street, hospital_name, insurance_company, phone_number, email FROM patient p
JOIN address a ON p.patient_id = a.address_id
JOIN hospital_has_patient q ON p.patient_id=q.patient_id
JOIN hospital h ON h.hospital_id=q.Hospital_id
JOIN insurance_company i ON i.insurance_company_id = p.insurance_company_id
JOIN contact c ON c.contact_id=p.contact_id
JOIN patient_has_doctor w ON p.patient_id=w.patient_id
JOIN doctor d ON d.doctor_id = w.doctor_id

JOIN patient_has_diagnose x ON p.patient_id=x.patient_id
JOIN diagnose di ON di.diagnose_id = x.diagnose_id

JOIN patient_in_department y ON p.patient_id=y.patient_id
JOIN department de ON de.department_id = y.department_id
------------------------------------------------------------------------------------


-- insert contact data
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '775266194', 'hrbek1@gmail.com');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '732828814', 'simonp@seznam.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '605120635', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '602202060', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '608177694', 'martinecvl@outlook.cz');	
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '772794587', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '725683201', 'kalinova@gmail.com');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '728686458', 'denisbrod@outlook.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '730309652', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '776634935', 'zlatahouloub@gmail.com');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '776225102', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '772199886', 'soltysjul@seznam.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '604072124', 'vackovasim@seznam.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '727950381', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '727003576', null);	
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '725316685', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '771833274', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '770429194', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '734529820', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '772070430', 'larvac@gmail.com');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '607409870', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '731527350', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '603220159', 'pilatt@gmail.com');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '736793644', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '733642276', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '732803870', 'ajirik@seznam.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '723173360', 'xkoub@gmail.com');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '725828673', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '737084035', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '723319300', 'strobl@gmail.com');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '738272699', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '779952837', 'schuster@outlook.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '729814057', 'plasilova@outlook.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '720079870', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '720072654', null);	
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '607981958', 'steflprem@gmail.com');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '727824365', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '731644803', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '723240295', 'roszachoval@gmail.com');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '728905380', null);						
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '608282958', 'vyoral3@seznam.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '608873319', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '738272651', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '737734409', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '607489343', null);					
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '777890911', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '608735304', 'krupicz@outlook.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '735897741', null);
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '736378620', 'kopeckova@gmail.com');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '608258602', 'drbelik@seznam.cz');	
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '601245273', 'katerinab25@gmail.com');

INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '725974644', 'petr.solar@vojnembrno.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '605478464', 'helena.randulova@nempardubice.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '608874124', 'julie.vondrackova@fnplzen.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '792154578', 'velky.simon@fnostrava.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '608746478', 'rychly.mir@onkladno.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '726899946', 'narozny.pav@njihlava.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '608779768', 'novak.hana@ntbzlin.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '765946896', 'modra.len@fnprague.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '735646116', 'jonas.cerny@vojnembrno.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '794615746', 'zeman.mich@fnprague.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '753315746', 'kocarova@seznam.cz');

INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '741258481', 'srna@gmail.com');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '608351246', 'cmelarikV@gmail.com');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '602751028', 'enavratilova@seznam.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '721467231', 'souckovasim@outlook.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '732261492', 'vlasorech@gmail.com');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '722347890', 'svatavska@outlook.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '731265222', 'komarekales@seznam.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '741602351', 'martinasekP@seznam.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '720560989', 'holesovavlasta@seznam.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '608620924', 'svestkamartin@gmail.com');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '733548753', 'krasnaM@seznam.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '734266173', 'brothankovaA@gmail.com');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '603151280', 'kratoskovaE@seznam.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '777426355', 'horskami@outlook.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '724686547', 'karelpavlicek@gmail.com');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '604206876', 'berkovamarta@seznam.cz');
INSERT INTO public.contact (contact_id, phone_number, email) VALUES (nextval('contact_contact_id_seq'), '606743265', 'berkamartin@seznam.cz');



-- insert insurance_company data
INSERT INTO public.insurance_company (insurance_company_id, insurance_company, insurance_company_code) VALUES (nextval('insurance_company_insurance_company_id_seq'), 'Všeobecná zdravotní pojišťovna ČR', 111);
INSERT INTO public.insurance_company (insurance_company_id, insurance_company, insurance_company_code) VALUES (nextval('insurance_company_insurance_company_id_seq'), 'Vojenská zdravotní pojišťovna ČR', 201);
INSERT INTO public.insurance_company (insurance_company_id, insurance_company, insurance_company_code) VALUES (nextval('insurance_company_insurance_company_id_seq'), 'Česká průmyslová zdravotní pojišťovna', 205);
INSERT INTO public.insurance_company (insurance_company_id, insurance_company, insurance_company_code) VALUES (nextval('insurance_company_insurance_company_id_seq'), 'Oborová zdravotní pojišťovna', 207);
INSERT INTO public.insurance_company (insurance_company_id, insurance_company, insurance_company_code) VALUES (nextval('insurance_company_insurance_company_id_seq'), 'Zaměstnanecká pojišťovna Škoda', 209);
INSERT INTO public.insurance_company (insurance_company_id, insurance_company, insurance_company_code) VALUES (nextval('insurance_company_insurance_company_id_seq'), 'Zdravotní pojišťovna ministerstva vnitra ČR', 211);
INSERT INTO public.insurance_company (insurance_company_id, insurance_company, insurance_company_code) VALUES (nextval('insurance_company_insurance_company_id_seq'), 'RBP, zdravotní pojišťovna', 213);


-- insert patient data
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Erik', 'Hrbek', '1983-06-18', '830618/0993', 1, 5);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Šimon', 'Pejša', '1983-05-09', '830509/9044', 2, 4);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Leoš', 'Šikula', '2017-09-01', '170901/2316', 3, 1);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Evženie', 'Klabnová', '1999-03-03', '990303/3745', 4, 2);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Vlastislav', 'Martinec', '2000-12-06', '001206/0092', 5, 1);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Marcel', 'Šťovíček', '2012-08-05', '120805/5882', 6, 1);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Renáta', 'Kalinová', '2012-12-13', '126213/4742', 7, 2);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Denisa', 'Brodská', '2000-09-21', '005921/6069', 8, 3);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Magdaléna', 'Klásková', '1962-11-11', '626111/3067', 9, 6 );
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Zlata', 'Holoubková', '1983-03-15', '835315/7065', 10, 7);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Evelína', 'Fryčová', '1968-02-12', '685212/4213', 11, 5);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Julie', 'Šoltysová', '1993-10-16', '936016/0601', 12, 4);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Simona', 'Vacková', '1996-05-03', '965503/0363', 13, 2);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Eva', 'Patáková', '1974-05-20', '745620/7011', 14, 1);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Michaela', 'Pavlovská', '1975-06-18', '755618/2359', 15, 3);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Hubert', 'Benda', '1980-09-13', '800913/3858', 16, 1);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Servác', 'Rusek', '1976-09-14', '760914/9955', 17, 2);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Kvido', 'Vašíček', '1975-09-20', '750920/0193', 18, 3 );
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Bořek', 'Chromý', '1954-01-27', '540127/2525', 19, 5);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Václav', 'Lorenz', '2009-09-23', '090923/7604', 20, 4);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Otakar', 'Večerka', '1959-05-02', '590502/2024', 21, 6);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Igor', 'Grepl', '1977-10-22', '771022/7998', 22, 7);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Teodor', 'Pilát', '2006-10-23', '061023/0071', 23, 5);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Luboš', 'Vobořil', '1962-04-03', '620403/6817', 24, 3);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Viktorie', 'Štefanová', '1959-06-18', '595618/2760', 25, 1);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Alena', 'Jiříková', '1992-07-04', '925704/3653', 26, 3);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Marina', 'Halušková', '1968-05-17', '685517/9397', 27, 1);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Xenie', 'Koubková', '2003-03-26', '035326/8003', 28, 2);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Gita', 'Becková', '1977-04-03', '775403/4024', 29, 6);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Ida', 'Štroblová', '1998-07-20', '985720/8735', 30, 4);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Radka', 'Zelená', '1955-01-07', '555107/1141', 31, 1);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Blažena', 'Schusterová', '1992-07-22', '925722/2909', 32, 1);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Ivona', 'Plašilová', '2005-07-11', '055711/4360', 33, 3);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Eduard', 'Holec', '1979-01-25', '790125/8112', 34, 2);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Zbyšek', 'Stříž', '1973-03-19', '730319/2127', 35, 2);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Přemysl', 'Štefl', '1996-10-02', '961002/6129', 36, 4);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Ignác', 'Halíř', '1961-02-13', '610213/2289', 37, 4);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Vojtěch', 'Kubín', '1981-04-17', '810417/5970', 38, 5);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Rostislav', 'Zachoval', '2001-04-11', '010411/1579', 39, 6 );
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'David', 'Hradský', '1986-07-28', '860728/6215', 40, 7);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Vratislav', 'Řihák', '1964-08-06', '640806/2661', 41, 7);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Pravoslav', 'Vyoral', '2002-02-19', '020219/5059', 42, 2);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Julius', 'Kavan', '1966-03-07', '660307/5798', 43, 1);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Vlastislav', 'Pour', '1974-11-22', '741122/0267', 44, 4);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Richard', 'Štefek', '1972-02-16', '720216/8171', 45, 5);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Miluše', 'Daníčková', '1959-04-22', '595422/1273', 46, 6);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Žaneta', 'Krupičková', '2008-07-14', '085714/8655', 47, 2);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Gizela', 'Kinclová', '1976-12-08', '766208/0547', 48, 4);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Vendula', 'Kopečková', '1991-07-03', '915703/9628', 49, 3);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Drahoslava', 'Bělíková', '1992-05-17', '925517/7998', 50, 1);
INSERT INTO public.patient (patient_id, name, surname, date_of_birth, personal_no, contact_id, insurance_company_id) VALUES (nextval('patient_patient_id_seq'), 'Kateřina', 'Bydžovská', '1993-05-08', '935508/6455', 51, 2);

-- insert address data
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Plzeň','Stožická','13101','30100');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Brno','Partyzánská','735','61200');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Pardubice','Poštovní','559','53003');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Kladno','K Pazderně','29','27201');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Jihlava','Palackého','782','58601');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Ostrava','Opltova','83','70030');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Zlín','Vostelčická','1137','76001');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Praha','Jiřího z Poděbrad','88','10000');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Brno','Štefánikova','1231','60300');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Praha','Pivovarská','1254','10400');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Plzeň','Obránců míru','15103','30100');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Pardubice','Hrubínova','4106','53003');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Most','Havlíčkova','921','43401');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Liberec','Žižkova','1189','46014');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Liberec','Havlíčkova','1330','46014');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'České Budějovice','Stožická','1383','37004');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Olomouc','Příční','8133','78365');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Hradec Králové','Karafiátova','674','50003');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'České Budějovice','Mlýnská','117','37004');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Olomouc','Československých legií','1880','78365');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Opava','Družstevní','1477','74601');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Brno','Petrželka','1084','60300');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Brno','Jiráskova','681','61200');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Zlín','Na Loučkách','1153','76001');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Praha','Na Výsluní','1523','10000');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Brno','Jiráskova','1995','60300');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Jihlava','Jabloňová','1662','58601');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Most','Tylova','1756','43401');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Kladno','Jirsíkova','5136','27201');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Hradec Králové','Pivovarská','1874','50003');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Praha','Kovářská','387','10400');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Brno','Na Loučkách 962','','61200');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Zlín','Žižkova','1899','76001');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Jihlava','Sovova','1240','58601');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'České Budějovice','Polní','1433','37004');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Plzeň','Zahradní','1113','31200');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Opava','Nádražní','513','74601');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Most','Tylova','1409','43401');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Pardubice','Stožická','1027','53002');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Praha','Fibichova','464','10100');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Brno','Českého odboje','1503','61200');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Olomouc','Nádražní','226','78365');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Ostrava','Nepomuckého','203','70030');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Praha','Na Výsluní','586','10400');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Ostrava','Lidická','1254','70030');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Most','Nepomuckého','205','43401');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Jihlava','Nádraží','308','58601');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Praha','Tyršova','1021','10000');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Jihlava','Hamerská','1346','58601');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Brno','Strojírenská','1397','60300');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Kladno','Tylova','1692','27201');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Brno','Zábrdovická','3/3','61500');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'České Budějovice','Boženy Němcové','585/54','37001');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Hradec Králové','Sokolská','581','50005');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Jihlava','Vrchlického','59','58601');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Kladno','Vančurova','1548','27201');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Liberec','Husova','10','46001');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Most','Jana Evangelisty Purkyně','270','43464');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Olomouc','I.P.Pavlova','185','77900');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Ostrava','17. listopadu','1790/5','70800');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Opava','Olomoucká','470/86','74601');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Pardubice','Kyjevská','44','53003');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Plzeň','Alej Svobody','80','32300');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Praha','U Nemocnice','499/2','12808');
INSERT INTO public.address (address_id, city, street, house_number, zip_code) VALUES (nextval('address_address_id_seq'), 'Zlín','Havlíčkovo nábřeží','600','76275');


-- insert patient_has_address data
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (1,1);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (2,2);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (3,3);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (4,4);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (5,5);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (6,6);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (7,7);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (8,8);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (9,9);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (10,10);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (11,11);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (12,12);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (13,13);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (14,14);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (15,15);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (16,16);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (17,17);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (18,18);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (19,19);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (20,20);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (21,21);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (22,22);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (23,23);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (24,24);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (25,25);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (26,26);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (27,27);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (28,28);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (29,29);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (30,30);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (31,31);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (32,32);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (33,33);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (34,34);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (35,35);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (36,36);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (37,37);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (38,38);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (39,39);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (40,40);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (41,41);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (42,42);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (43,43);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (44,44);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (45,45);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (46,46);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (47,47);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (48,48);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (49,49);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (50,50);
INSERT INTO public.patient_has_address (patient_id, address_id) VALUES (51,51);


-- insert hospital data
INSERT INTO public.hospital (hospital_id, hospital_name, address_id) VALUES (nextval('hospital_hospital_id_seq'), 'Vojenská nemocnice Brno','52');
INSERT INTO public.hospital (hospital_id, hospital_name, address_id) VALUES (nextval('hospital_hospital_id_seq'), 'Nemocnice České Budějovice','53');
INSERT INTO public.hospital (hospital_id, hospital_name, address_id) VALUES (nextval('hospital_hospital_id_seq'), 'Fakultní nemocnice Hradec Králové','54');
INSERT INTO public.hospital (hospital_id, hospital_name, address_id) VALUES (nextval('hospital_hospital_id_seq'), 'Nemocnice Jihlava','55');
INSERT INTO public.hospital (hospital_id, hospital_name, address_id) VALUES (nextval('hospital_hospital_id_seq'), 'Oblastní nemocnice Kladno','56');
INSERT INTO public.hospital (hospital_id, hospital_name, address_id) VALUES (nextval('hospital_hospital_id_seq'), 'Krajská nemocnice Liberec','57');
INSERT INTO public.hospital (hospital_id, hospital_name, address_id) VALUES (nextval('hospital_hospital_id_seq'), 'Nemocnice Most','58');
INSERT INTO public.hospital (hospital_id, hospital_name, address_id) VALUES (nextval('hospital_hospital_id_seq'), 'Fakultní nemocnice Olomouc','59');
INSERT INTO public.hospital (hospital_id, hospital_name, address_id) VALUES (nextval('hospital_hospital_id_seq'), 'Fakultní nemocnice Ostrava','60');
INSERT INTO public.hospital (hospital_id, hospital_name, address_id) VALUES (nextval('hospital_hospital_id_seq'), 'Slezská nemocnice v Opavě','61');
INSERT INTO public.hospital (hospital_id, hospital_name, address_id) VALUES (nextval('hospital_hospital_id_seq'), 'Pardubická nemocnice','62');
INSERT INTO public.hospital (hospital_id, hospital_name, address_id) VALUES (nextval('hospital_hospital_id_seq'), 'Fakultní nemocnice Plzeň','63');
INSERT INTO public.hospital (hospital_id, hospital_name, address_id) VALUES (nextval('hospital_hospital_id_seq'), 'Všeobecná fakultní nemocnice v Praze','64');
INSERT INTO public.hospital (hospital_id, hospital_name, address_id) VALUES (nextval('hospital_hospital_id_seq'), 'Krajská nemocnice Tomáše Bati Zlín','65');


-- insert hospital_has_patient data
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (1,2);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (1,9);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (1,22);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (1,23);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (1,26);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (1,32);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (1,41);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (1,50);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (2,16);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (2,19);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (2,35);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (3,18);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (3,30);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (4,5);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (4,27);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (4,34);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (4,47);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (4,49);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (5,4);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (5,29);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (5,51);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (6,14);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (6,15);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (7,13);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (7,28);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (7,38);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (7,46);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (8,17);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (8,20);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (8,42);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (9,6);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (9,43);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (9,45);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (10,21);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (10,37);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (11,3);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (11,12);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (11,39);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (12,1);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (12,11);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (12,36);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (13,8);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (13,10);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (13,25);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (13,31);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (13,40);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (13,44);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (13,48);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (14,7);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (14,24);
INSERT INTO public.hospital_has_patient (hospital_id, patient_id) VALUES (14,33);




-- insert doctor data
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Petr', 'Solař', 52);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Helena', 'Randulová', 53);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Julie', 'Vondráčková', 54);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Šimon', 'Velký', 55);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Miroslav', 'Rychlý', 56);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Pavel', 'Nárožný', 57);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Hana', 'Nováková', 58);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Lenka', 'Modrá', 59);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Jonáš', 'Černý', 60);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Michal', 'Zeman', 61);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Marcela', 'Kočárova', 62);--nikdo

INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Zikmund', 'Srna', 63);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Václav', 'Čmelařík', 64);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Edita', 'Navrátilová', 65);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Simona', 'Součková', 66);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Vlastimil', 'Ořech', 67);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Drahomíra', 'Svatavská', 68);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Aleš', 'Komárek', 69);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Pavel', 'Martinásek', 70);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Vlasta', 'Holešová', 71);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Martin', 'Švestka', 72);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Martina', 'Krásná', 73);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Anežka', 'Brothánková', 74);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Ester', 'Krátošková', 75);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Milada', 'Horská', 76);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Karel', 'Pavlíček', 77);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Marta', 'Berková', 78);
INSERT INTO public.doctor (doctor_id, doctor_name, doctor_surname, contact_id) VALUES (nextval('doctor_doctor_id_seq'), 'Martin', 'Berka', 79);


-- instert patient_has_doctor
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (1, 3);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (2, 1);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (3, 2);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (4, 5);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (5, 6);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (6, 4);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (7, 7);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (8, 8);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (9, 9);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (10, 10);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (11, 3);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (12, 2);

INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (13, 11);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (14, 23);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (15, 24);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (16, 19);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (17, 25);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (18, 21);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (19, 20);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (20, 26);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (21, 27);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (22, 1);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (23, 9);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (24, 14);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (25, 8);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (26, 9);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (27, 15);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (28, 11);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (29, 5);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (30, 21);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (31, 10);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (32, 1);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (33, 7);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (34, 6);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (35, 20);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (36, 3);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (37, 27);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (38, 11);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (39, 13);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (40, 10);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (41, 1);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (42, 25);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (43, 17);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (44, 8);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (45, 17);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (46, 12);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (47, 15);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (48, 8);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (49, 6);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (50, 1);
INSERT INTO public.patient_has_doctor (patient_id, doctor_id) VALUES (51, 5);

-- insert diagnose data
INSERT INTO public.diagnose (diagnose_id, diagnose) VALUES (nextval('diagnose_diagnose_id_seq'), 'akutní apendicitida');
INSERT INTO public.diagnose (diagnose_id, diagnose) VALUES (nextval('diagnose_diagnose_id_seq'), 'nekompenzovaný DM II. typu');
INSERT INTO public.diagnose (diagnose_id, diagnose) VALUES (nextval('diagnose_diagnose_id_seq'), 'TEP dextra genus');
INSERT INTO public.diagnose (diagnose_id, diagnose) VALUES (nextval('diagnose_diagnose_id_seq'), 'gravidita');
INSERT INTO public.diagnose (diagnose_id, diagnose) VALUES (nextval('diagnose_diagnose_id_seq'), 'schizofrenie');
INSERT INTO public.diagnose (diagnose_id, diagnose) VALUES (nextval('diagnose_diagnose_id_seq'), 'CMP');
INSERT INTO public.diagnose (diagnose_id, diagnose) VALUES (nextval('diagnose_diagnose_id_seq'), 'TBC');
INSERT INTO public.diagnose (diagnose_id, diagnose) VALUES (nextval('diagnose_diagnose_id_seq'), 'leukemie');
INSERT INTO public.diagnose (diagnose_id, diagnose) VALUES (nextval('diagnose_diagnose_id_seq'), 'renální kolika');
INSERT INTO public.diagnose (diagnose_id, diagnose) VALUES (nextval('diagnose_diagnose_id_seq'), 'nefrolithiáza');--nikdo 10
INSERT INTO public.diagnose (diagnose_id, diagnose) VALUES (nextval('diagnose_diagnose_id_seq'), 'tříselná kýla');
INSERT INTO public.diagnose (diagnose_id, diagnose) VALUES (nextval('diagnose_diagnose_id_seq'), 'fraktura femuru');

-- insert patient_has_diagnose data
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (1, 2);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (2, 2);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (3, 1);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (4, 4);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (5, 5);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (6, 3);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (7, 6);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (8, 7);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (9, 8);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (10, 9);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (11, 11);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (12, 12);

INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (13, 9);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (14, 12);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (15, 8);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (16, 9);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (17, 3);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (18, 11);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (19, 7);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (20, 5);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (21, 2);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (22, 2);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (23, 8);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (24, 6);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (25, 7);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (26, 8);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (27, 11);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (28, 9);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (29, 4);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (30, 1);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (31, 9);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (32, 2);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (33, 4);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (34, 5);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (35, 7);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (36, 12);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (37, 12);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (38, 9);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (39, 8);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (40, 9);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (41, 2);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (42, 3);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (43, 7);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (44, 7);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (45, 7);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (46, 4);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (47, 1);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (48, 7);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (49, 5);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (50, 2);
INSERT INTO public.patient_has_diagnose (patient_id, diagnose_id) VALUES (51, 4);

-- insert department data
INSERT INTO public.department (department_id, department, floor) VALUES (nextval('department_department_id_seq'), 'chirurgie', 3);
INSERT INTO public.department (department_id, department, floor) VALUES (nextval('department_department_id_seq'), 'interna', 6);
INSERT INTO public.department (department_id, department, floor) VALUES (nextval('department_department_id_seq'), 'ortopedie', 7);
INSERT INTO public.department (department_id, department, floor) VALUES (nextval('department_department_id_seq'), 'interna', 5);
INSERT INTO public.department (department_id, department, floor) VALUES (nextval('department_department_id_seq'), 'gynokologie', 7);
INSERT INTO public.department (department_id, department, floor) VALUES (nextval('department_department_id_seq'), 'psychiatrie', 1);
INSERT INTO public.department (department_id, department, floor) VALUES (nextval('department_department_id_seq'), 'neurologie', 4);
INSERT INTO public.department (department_id, department, floor) VALUES (nextval('department_department_id_seq'), 'pneumonologie', 2);
INSERT INTO public.department (department_id, department, floor) VALUES (nextval('department_department_id_seq'), 'hematologie', 10);
INSERT INTO public.department (department_id, department, floor) VALUES (nextval('department_department_id_seq'), 'urologie', 5);
INSERT INTO public.department (department_id, department, floor) VALUES (nextval('department_department_id_seq'), 'chirurgie', 4);


-- insert patient_in_department data
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (1, 2, '2015-2-23');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (2, 4, '2021-9-20');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (3, 1, '2018-12-2');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (4, 5, '2021-8-1');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (5, 6, '2021-10-25');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (6, 3, '2020-3-5');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (7, 7, '2021-10-22');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (8, 8, '2021-8-10');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (9, 9, '2021-5-11');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (10, 10, '2021-3-12');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (11, 11, '2021-7-27');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (12, 1, '2021-1-23');

INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (13, 10, '2020-12-23');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (14, 4, '2021-1-2');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (15, 9, '2021-4-20');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (16, 10, '2021-7-3');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (17, 3, '2021-9-14');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (18, 11, '2020-8-1');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (19, 8, '2021-5-23');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (20, 6, '2021-11-17');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (21, 4, '2021-1-9');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (22, 2, '2021-9-4');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (23, 9, '2021-7-2');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (24, 5, '2021-4-30');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (25, 8, '2021-7-31');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (26, 9, '2021-3-12');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (27, 1, '2021-6-18');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (28, 10, '2021-1-27');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (29, 5, '2021-6-8');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (30, 11, '2021-9-26');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (31, 10, '2021-4-14');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (32, 2, '2021-1-3');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (33, 7, '2021-1-23');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (34, 6, '2021-1-29');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (35, 8, '2021-5-1');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (36, 4, '2021-4-3');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (37, 4, '2020-11-23');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (38, 10, '2020-12-30');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (39, 9, '2021-1-1');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (40, 10, '2020-12-27');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (41, 2, '2020-11-12');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (42, 3, '2021-4-21');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (43, 8, '2021-8-11');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (44, 8, '2021-4-13');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (45, 8, '2021-7-19');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (46, 5, '2021-5-11');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (47, 1, '2021-1-28');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (48, 8, '2021-2-20');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (49, 6, '2021-8-10');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (50, 2, '2021-6-5');
INSERT INTO public.patient_in_department (patient_id, department_id, date) VALUES (51, 5, '2021-7-2');
